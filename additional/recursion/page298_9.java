package recursion;

public class page298_9 {
	public static void reverseDisplay(String value){
		String s = "";
		if(value.length() <= 0){
			return;
		}else{
			s = s.concat(value.charAt(value.length()-1)+"");
			System.out.print(s);
			reverseDisplay(value.substring(0, value.length()-1));
		}
	}
	
	public static void main(String[] args){
		reverseDisplay("abcd");
	}
}
