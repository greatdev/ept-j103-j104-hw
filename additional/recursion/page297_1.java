package recursion;

import java.math.BigInteger;

public class page297_1 {
	
	public static BigInteger fac(BigInteger a){
		if(a.compareTo(BigInteger.ONE) == 0){
			return BigInteger.ONE;
		}else{
			return a.multiply(fac(a.subtract(BigInteger.ONE)));
		}
	}
	
	public static void main(String[] args) {
		System.out.println(fac(BigInteger.valueOf(4)));
	}
}
