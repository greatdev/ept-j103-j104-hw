package recursion;

public class page299_10 {
	
	static int c = 0;
	public static int count(String str, char a){
		if(str.length() <= 0){
			return c;
		}else{
			if(str.charAt(0) == a){
				c++;
			}
			return count(str.substring(1), a);
		}
		
	}
	
	public static void main(String[] a){
		System.out.println(count("Welcomeeee", 'e'));
	}
}
