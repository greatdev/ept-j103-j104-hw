package recursion;

public class page299_13 {
	static int max = Integer.MIN_VALUE;
	static int count = 0;
	public static int findMax(int[] a) {
		if (count == a.length) {
			return max;
		} else {
			if(a[count] > max){
				max = a[count];
			}
			count++;
 			return findMax(a);
		}

	}

	public static void main(String[] a) {
		int[] arr = {1,7,3,56,2};
		System.out.println(findMax(arr));

	}
}
