package recursion;

public class page298_3 {
	// GCD using euclid algorithm
	public static int gcd(int m, int n) {
		if ((m % n) == 0)
			return n;
		else
			return gcd(n, m % n);
	}
	
	public static void main(String[] args){
		System.out.println(gcd(468,24));
	}
}
