package recursion;

public class page298_8 {
	public static void reverseDisplay(int value){
		if(value == 0){
			return;
		}else{
			System.out.print(value%10);
			reverseDisplay(value/10);
		}
	}
	
	public static void main(String[] args){
		reverseDisplay(12345);
	}
}
