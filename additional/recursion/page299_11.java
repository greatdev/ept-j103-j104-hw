package recursion;

public class page299_11 {

	static int c = 0;

	public static int sumDigits(int a) {
		if (a<=0) {
			return c;
		} else {
			c += a%10;
			return sumDigits(a/10);
		}

	}

	public static void main(String[] a) {
		System.out.println(sumDigits(234));

	}
}
