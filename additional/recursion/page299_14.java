package recursion;

public class page299_14 {
	static int count = 0;
	public static int upperCase(String a) {
		if(a.length() <= 0){
			return count;
		}else{
			int ascii = (int)a.charAt(0);
			if(ascii >= 65 && ascii <= 90){
				count++;
			}
			return upperCase(a.substring(1));
		}

	}

	public static void main(String[] a) {
		String ab = "aaAcDSEf";
		System.out.println(upperCase(ab));
	}
}
