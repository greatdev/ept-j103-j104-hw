package method;

public class Page294_8 {
	
	public static double salesCommission(double amount, double rate){
		double temp = amount*(rate/100);
		return temp;
	}
	
	public static double squareRoot(double a){
		return Math.sqrt(a);
	}
	
	public static boolean isEven(int a){
		return a%2==0?true:false;
	}
	
	public static double monthlyPayment(double loan, int number_of_year, double interest_rate){
		int n = 12*number_of_year;
		double c = interest_rate/12;
//		http://money.stackexchange.com/questions/61639/what-is-the-formula-for-the-monthly-payment-on-an-adjustable-rate-mortgage
		double result = loan*((c*Math.pow(1+c, n))/(Math.pow(1+c, n)-1));
		return result;
	}
	
	
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
