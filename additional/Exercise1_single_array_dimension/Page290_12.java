package Exercise1_single_array_dimension;

import java.util.Arrays;

public class Page290_12 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double[] d = {1.1,2.2,3.3};
		double[] t = new double[d.length];
 		for(int i = 0; i< d.length ;i++ ){
 			t[i] = d[i];
 		}
 		
 		System.out.println(Arrays.toString(t));
	}

}
