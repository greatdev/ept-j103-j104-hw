package Exercise1_single_array_dimension;

import java.util.Arrays;

public class Page291_21_selection_sort {
	
	public static int[] selectionSort(int[] x){
		for(int j = 0; j<x.length;j++){
			int min = Integer.MAX_VALUE;
			int min_index = -1;
			int select_point = x.length-j;
			for(int i = 0; i< select_point ;i++){
				if(x[i] < min){ //decrease
					min = x[i];
					min_index  = i;
				}
			}
			int temp = x[select_point-1];
			x[select_point-1] = x[min_index];
			x[min_index] = temp;
		}
		return x;
	}
	
	public static void main(String[] args) {
		int[] x = {2,4,6,3,6,9};
		selectionSort(x);
		System.out.println(Arrays.toString(x));
	}

}
