package Exercise1_single_array_dimension;

public class Page289_8 {

	public static void main(String[] args) {
		int[] a = new int[5];
		a[0] = 10;
		System.out.println(a[0]+a[a.length-1]);
		
		double sum = 0;
		int max = Integer.MIN_VALUE;
		for(int i = 0 ;i<a.length;i++){
			sum+= a[i];
			if(max < a[i]){
				max = a[i];
			}
		}
		System.out.println(sum/a.length);
		System.out.println(max);
		System.out.println(a[a.length-2]);
		
		int[] b = new int[]{1,2,3,4,5};

	}

}
