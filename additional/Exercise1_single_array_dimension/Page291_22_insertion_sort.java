package Exercise1_single_array_dimension;

import java.util.Arrays;

public class Page291_22_insertion_sort {

	public static int[] insertionSort(int[] a) {
		for (int i = 1; i < a.length; i++) {
			int temp = a[i];
			int j;
			for (j = i - 1; j >= 0 && temp > a[j]; j--) {
				a[j + 1] = a[j];
			}
			a[j + 1] = temp;
		}
		
		return a;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] x = {2,4,6,3,6,9};
		insertionSort(x);
		System.out.println(Arrays.toString(x));
	}

}
