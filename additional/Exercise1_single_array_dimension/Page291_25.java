package Exercise1_single_array_dimension;

import java.util.Arrays;

public class Page291_25 {
	public static void main(String[] args) {
		int[] l1 = {12,14,17,20};
		int[] l2 = {12,14,17,20};
		System.out.println(Arrays.equals(l1, l2));
		
		Arrays.fill(l1, 1,3,18); //postion 1 to 3 equal 18
		System.out.println(Arrays.toString(l1));
	}
}
