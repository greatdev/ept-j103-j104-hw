package Exercise1_single_array_dimension;

public class Page291_16_max {
	
	public static double max(double a, double b, double c,double d){
		double max = 0;
		if(a>b && a>c && a>d){
			max = a;
		}else if(b>a && b>c && b>d){
			max = b;
		}else if(c>a && c>b && c>d){
			max = c;
		}else{
			max = d;
		}
		return max;
	}
	
	public static double max(float[] x){
		double max = Integer.MIN_VALUE;
		for(int i = 0; i<x.length;i++){
			if(max < x[i]){
				max = x[i];
			}
		}
		return max;
	}
	
	public static long max(long[] x){
		long max = Integer.MIN_VALUE;
		for(int i = 0; i<x.length;i++){
			if(max < x[i]){
				max = x[i];
			}
		}
		return max;
	}
	
	public static void main(String[] args) {
		
		System.out.println(max(1.1,2.2,3.3,4.4));
		System.out.println(max(new float[]{1,2,3}));
		System.out.println(max(new long[]{1,2,3}));
	}

}
