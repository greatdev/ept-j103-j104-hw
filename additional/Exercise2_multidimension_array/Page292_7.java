package Exercise2_multidimension_array;

public class Page292_7 {
	
	public static int[] ml(int[][] m){
		int[] result = new int[2];
		result[0] = m.length;
		result[1] = m[0].length;
		return result;
	}
	
	public static void main(String[] args) {
		int[][] array = {{1,2,3,4}, {5,6,7,8,9}};
		System.out.println(ml(array)[0]);
		System.out.println(ml(array)[1]);
	}
}
