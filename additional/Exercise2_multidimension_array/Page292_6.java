package Exercise2_multidimension_array;

public class Page292_6 {

	public static void main(String[] args) {
		int[][] array = {{5,6},{8,5},{9,8}};
		int product = 1;
		for(int i = 0;i < array.length;i++){
			product *= array[i][0];
		}
		System.out.println(product);
	}
}
