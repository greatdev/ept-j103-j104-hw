package Exercised13;

public class Page285_Rectangle {
	
	private double width = 0;
	private double height = 0;
	private Point p;
	
	public Page285_Rectangle(double x, double y, double w, double h) {
		p = new Point(x,y);
		width = p.x + w;
		height = p.x + h;
	}
	
	public double getArea(){
		return width*height;
	}
	
	public double getPerimeter(){
		return width*2 + height*2;
	}

}
