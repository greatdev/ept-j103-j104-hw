package Exercised13;

public class Page286_11_PiggyBank {
	
	private int coin1;
	private int coin2;
	private int coin5;
	private int coin10;
	
	public Page286_11_PiggyBank() {
		coin1 = 0;
		coin2 = 0;
		coin5 = 0;
		coin10 = 0;
	}
	
	public void add1(int c){
		coin1 += c;
	}
	
	public void add2(int c){
		coin2 += (2*c);
	}
	
	public void add5(int c){
		coin5 += (5*c);
	}
	
	public void add10(int c){
		coin10 += (10*c);
	}
	
	public void clear(){
		coin1 = 0;
		coin2 = 0;
		coin5 = 0;
		coin10 = 0;
	}
	
	public int getTotal(){
		return coin1+coin2+coin5+coin10;
	}
	
	public String toString(){
		return "coin 1 = "+coin1+", "+"coin 2 = "+coin2+", "+"coin 5 = "+coin5+", "+"coin 10 = "+coin10;
	}
	
	
}
