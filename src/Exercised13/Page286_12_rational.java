package Exercised13;

public class Page286_12_rational {
	private int numerator;
	private int denominator;
	
	public static final int zero = 0;
	public static final int one = 1;
	
	public Page286_12_rational() {
		numerator = zero;
		denominator = one;
	}
	
	public Page286_12_rational(int n) {
		numerator = n;
		denominator = 1;
	}
	
	public Page286_12_rational(int n, int d) {
		numerator = n;
		denominator = d;
	}
	
	public int getNumerator(){
		return numerator;
	}
	
	public int getDenominator(){
		return denominator;
	}
	
	public Page286_12_rational add(Page286_12_rational R){
		int num = this.getNumerator()*R.getDenominator() + this.getDenominator()*R.getNumerator();
		int denom = this.getDenominator()*R.getDenominator();
		
		return new Page286_12_rational(num,denom);
	}
	
	public Page286_12_rational sub(Page286_12_rational R){
		int num = this.getNumerator()*R.getDenominator() - this.getDenominator()*R.getNumerator();
		int denom = this.getDenominator()*R.getDenominator();
		
		return new Page286_12_rational(num,denom);
	}
	
	public Page286_12_rational mul(Page286_12_rational R){
		int num = this.getNumerator() * R.getNumerator();
		int denom = this.getDenominator()*R.getDenominator();
		
		return new Page286_12_rational(num,denom);
	}
	
	public Page286_12_rational div(Page286_12_rational R){
		int num = this.getNumerator() * R.getDenominator();
		int denom = this.getDenominator()*R.getNumerator();
		
		return new Page286_12_rational(num,denom);
	}
	
	public Page286_12_rational inv(Page286_12_rational R){
		return new Page286_12_rational(R.getDenominator(),R.getNumerator());
	}
	
	public int gcd(int a, int b){
		int max = Integer.MIN_VALUE;
		if(a > b){
			max = a;
		}else{
			max = b;
		}
		
		while(true){
			if(a%max == 0 && b%max == 0) break;
			max--;
		}
		return max;
	}
	
	public boolean equals(Page286_12_rational R){
		// this
		int this_gcd = gcd(this.getNumerator(), this.getDenominator());
		int this_a = this.getNumerator()/this_gcd;
		int this_b = this.getDenominator()/this_gcd;
		
		int r_gcd = gcd(R.getNumerator(), R.getDenominator());
		int r_a = this.getNumerator()/r_gcd;
		int r_b = this.getDenominator()/r_gcd;
		
		if(this_a==r_a && this_b==r_b){
			return true;
		}else{
			return false;
		}
	}
	
	public String toString(){
		return this.getNumerator()+"/"+this.getDenominator();
	}
	
}
