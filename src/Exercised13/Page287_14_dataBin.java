package Exercised13;

import java.util.ArrayList;

public class Page287_14_dataBin {
	
	ArrayList<Double> DataCollection;
	
	public Page287_14_dataBin(){
		DataCollection = new ArrayList<Double>();
	}
	
	public void add(double x){
		DataCollection.add(x);
	}
	
	public int size(){
		return DataCollection.size();
	}
	
	public double getSum(){
		double sum = 0;
		for(int i = 0; i < this.size() ;i++){
			sum += DataCollection.get(i);
		}
		return sum;
	}
	
	public double Mean(){
		double result = this.getSum()/this.size();
		return result;
	}
	
	public double getSD(){
		double sigma_x = this.getSum();
		double sigma_x_sqr = 0;
		for(int i = 0; i < this.size() ;i++){
			sigma_x_sqr += Math.pow(DataCollection.get(i), 2);
		}
		double n = this.size();
		
		double a = ((n*sigma_x_sqr)-Math.pow(sigma_x, 2)) / (n*(n-1));
		double result = Math.sqrt(a);
		return result;
	}
	
	public double[] getData(){
		double[] data = new double[this.size()];
		for(int i = 0;i< this.size();i++){
			data[i] = DataCollection.get(i);
		}
		
		return data;
	}
	
}
