package Exercised13;

import java.math.BigInteger;

public class Page284_1_biginteger {
	
	public static BigInteger fibonacci2(int n) {
	    if (n == 0 || n == 1) {
	        return BigInteger.ONE;
	    }
	    return fibonacci2(n - 2).add(fibonacci2(n - 1));
	}

	public static void main(String[] args) {
	    for (int i = 0; i < 100; i++) {
	        System.out.println(fibonacci2(i));
	    }
	}

}
