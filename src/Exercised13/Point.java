package Exercised13;

// implement ว่า clone ได้
public class Point implements Cloneable{
	
	public double x = 0;
	public double y = 0;
	public Point() {
		// TODO Auto-generated constructor stub
	}
	
	public Point(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	public String toString(){
		return String.format("%f, %f", x,y);
	}
	
	//เช็คแบบลึก เช็คค่าใน object ด้วย
	public boolean equals(Object o){
		// o เป็น Object จาก class point ? 
		if(o instanceof Point){
			Point temp = (Point) o;
			return temp.x == x && temp.y == y;
		}else{
			return false;
		}
	}
	
	// การ clone แบบ 1
	public Point copyAll(){
		return new Point(this.x,this.y);
	}
	
	// การ clone แบบ 2 ดีกว่าแบบแรก เพราะกรณีมีตัวแปล field เยอะ เหนื่อย clone ดีกว่า
	public Object clone() throws CloneNotSupportedException{
		return super.clone();
	}
}
