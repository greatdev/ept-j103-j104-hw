package Exercised13;

public class Line {
	
	private double m;
	private double b;
	
	public Line(double m, double b) {
		this.m = m;
		this.b = b;
	}
	
	public Line(Line line){
		this.m = line.getM();
		this.b = line.getB();
	}
	
	public double getM(){
		return m;
	}
	
	public double getB(){
		return b;
	}
	
	public double getX(double x){
		return (m*x)+b;
	}
	
	public double getY(double y){
		return (y-b)/m;
	}
	
	public boolean isParallellTo(Line line){
		if(line.getM()==this.getM()){
			return true;
		}else{
			return false;
		}
	}
	
	public boolean contains(Point p){
		double x = this.getM()*p.x + this.getB();
		if(p.y == x){
			return true;
		}else{
			return false;
		}
	}
	
	public boolean isPerpendicularTo(Line line){
		if(line.getM()*this.getM() == -1){
			return true;
		}else{
			return false;
		}
	}
	
	public Point intersection(Line line){
		double x = (line.getB()-this.getB())/(this.getM()-line.getB());
		double y = this.getM()*x+this.getB();
		Point p = new Point(x,y);
		return p;
	}
	
	public boolean equals(Line line){
		if(line.getB() == this.getB() && line.getM() == this.getM()){
			return true;
		}else{
			return false;
		}
	}
	
	public String toString(){
		return "y = "+this.getM()+"x + "+this.getB();
	}
}
