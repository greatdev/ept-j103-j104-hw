package Exercised13;

import java.util.stream.IntStream;

public class Page285_13_set {

	private int[] set;
	private int count = 0;
	private int size;

	public Page285_13_set(int n) {
		size = n;
		set = new int[n];
	}

	public void add(int x) {
		if (count < size) {
			set[count++] = x;
		}
	}

	public boolean contains(int x) {
		return IntStream.of(set).anyMatch(a -> a == x);
	}

	public int[] getSet() {
		return set;
	}

	public int getSet(int i) {
		return set[i];
	}

	public boolean isSubsetOf(Page285_13_set s) {
		for (int i = 0; i < s.getSet().length; i++) {
			if (!this.contains(s.getSet(i))) {
				return false;
			}
		}
		return true;
	}

	public Page285_13_set union(Page285_13_set s) {
		int new_size = this.getSet().length + s.getSet().length;
		Page285_13_set new_s = new Page285_13_set(new_size);

		for (int i = 0; i < this.getSet().length; i++) {
			new_s.add(this.getSet(i));
		}

		for (int i = 0; i < s.getSet().length; i++) {
			new_s.add(s.getSet(i));
		}

		return new_s;

	}

	public Page285_13_set intersection(Page285_13_set s) {
		int c = 0;

		for (int i = 0; i < this.getSet().length; i++) {
			if (s.contains(this.getSet(i))) {
				c++;
			}
		}
		Page285_13_set new_s = new Page285_13_set(c);
		for (int i = 0; i < this.getSet().length; i++) {
			if (s.contains(this.getSet(i))) {
				new_s.add(this.getSet(i));
			}
		}
		return new_s;
	}

	public boolean equals(Page285_13_set s) {
		for (int i = 0; i < s.getSet().length; i++) {
			if (this.getSet(i) != s.getSet(i)) {
				return false;
			}
		}

		return true;
	}

	public String toString() {
		String a = "{ ";
		for (int i = 0; i < this.getSet().length; i++) {
			a += this.getSet(i) + ", ";
		}
		a += "}";
		return a;
	}

}
