package Exercised13;

public class Page287_15_RecentlyUsed {
	
	private String[] recently;
	private int count = 0;
	
	public Page287_15_RecentlyUsed(int n) {
		recently = new String[n];
	}
	
	public Page287_15_RecentlyUsed(String[] a) {
		recently = a;
	}
	
	public void add(String s){
		if(count == recently.length){
			for(int i = (recently.length-1); i>0 ;i--){
				recently[i] = recently[i-1];
			}
			recently[0] = s;
		}else{
			recently[count++] = s;
		}
	}
	
	public String get(int k){
		return recently[k];
	}
	
	public String[] toArray(){
		return recently;
	}
	
	public String toString(){
		String a = "";
		for(int i = 0;i<recently.length;i++){
			a += recently[i]+" ";
		}
		return a;
	}
}
