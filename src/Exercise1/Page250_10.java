package Exercise1;

public class Page250_10 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int year = 109;
		int yearDiff = 5;
		
		int DadAge, UncleAge;
		
		// ได้จากสูตร
		// DadAge+(DadAge+yearDiff) = year
		DadAge = (year-yearDiff)/2;
		UncleAge = DadAge+yearDiff;
		
		System.out.println("Dad's age is "+DadAge);
		System.out.println("Uncle's age is "+UncleAge);
		
	}

}
