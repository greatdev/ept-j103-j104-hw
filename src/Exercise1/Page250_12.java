package Exercise1;
import java.util.Scanner;

public class Page250_12 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.print("Input first: ");
		double a = sc.nextDouble();
		System.out.print("Input second: ");
		double b = sc.nextDouble();
		System.out.print("Input third: ");
		double c = sc.nextDouble();
		System.out.print("Input fourth: ");
		double d = sc.nextDouble();
		
		double p = a*d+c*b;
		double q = b*d;
		System.out.print("แบบไม่ต้องเป็นเศษส่วนอย่างต่ำ ");
		System.out.println(p+"/"+q);
		
		//using G.C.D
		double max,gcd;
		if(p>q){
			max = p;
		}else if(p<q){
			max = q;
		}else{
			// p = q
			System.out.print("แบบเศษส่วนอย่างต่ำ ");
			System.out.println(p/q);
			return;
		}
		
		while(true){
			if(p%max == 0 && q%max == 0) {
				gcd = max;
				break;
			}
			max--;
		}
		
		System.out.print("แบบเศษส่วนอย่างต่ำ ");
		System.out.println((p/gcd)+"/"+(q/gcd));
		
		
	}

}
