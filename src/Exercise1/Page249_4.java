package Exercise1;
import java.util.Scanner;

public class Page249_4 {
	// find the area of circle
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Input Radius : ");
		double r = sc.nextDouble();
		
		System.out.println(Math.PI*(r*r));
	}
}
