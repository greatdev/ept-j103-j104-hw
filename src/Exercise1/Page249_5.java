package Exercise1;
import java.util.Scanner;

public class Page249_5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.print("Input Inches : ");
		float inches = sc.nextFloat();
		
		// inch -> foot
		System.out.println("Feet : "+inches*(1/12.0)+" feet");
		
		//inches -> cm
		System.out.println("Centimeters : "+inches*2.54+" cm");
		
		// inches->yard
		System.out.println("Yards : "+inches*(1/36.0)+" yards");
	}

}
