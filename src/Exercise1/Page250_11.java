package Exercise1;
import java.util.Scanner;

public class Page250_11 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Input Width: ");
		double w = sc.nextDouble();
		System.out.print("Input Length: ");
		double l = sc.nextDouble();
		
		System.out.println("Area is "+(w*l));
		System.out.println("Circumference is "+ (2*w+2*l));
	}
}
