package Exercised10;

import java.util.Scanner;

public class Page277_7_triangle_type {

	public static boolean isEquilateral(double a, double b, double c) {
		if (a == b && b == c) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isIsosceles(double a, double b, double c) {
		if (a == b || b == c || a == c) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isRightangled(double a, double b, double c) {
		if (a * a + b * b == c * c) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isScalene(double a, double b, double c) {
		if (a != b || b != c || a != c) {
			return true;
		} else {
			return false;
		}
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("พิกัด x1 : ");
		int x1 = sc.nextInt();
		System.out.print("พิกัด y1 : ");
		int y1 = sc.nextInt();
		System.out.print("พิกัด x2 : ");
		int x2 = sc.nextInt();
		System.out.print("พิกัด y2 : ");
		int y2 = sc.nextInt();
		System.out.print("พิกัด x3 : ");
		int x3 = sc.nextInt();
		System.out.print("พิกัด y3 : ");
		int y3 = sc.nextInt();
		sc.close();
		
		//find distance
		double a = Math.sqrt(Math.pow((x1 - x2), 2) + Math.pow((y1 - y2), 2));
		double b = Math.sqrt(Math.pow((x2 - x3), 2) + Math.pow((y2 - y3), 2));
		double c = Math.sqrt(Math.pow((x1 - x3), 2) + Math.pow((y2 - y3), 2));

		double max = Integer.MIN_VALUE;
		double width, height;
		if (a > b && a > c) {
			max = a;
			width = b;
			height = c;
		} else if (b > a && b > c) {
			max = b;
			width = a;
			height = c;
		} else {
			max = c;
			width = a;
			height = b;
		}

		System.out.println(isEquilateral(width, height, max));
		System.out.println(isIsosceles(width, height, max));
		System.out.println(isRightangled(width, height, max));
		System.out.println(isScalene(width, height, max));

	}

}
