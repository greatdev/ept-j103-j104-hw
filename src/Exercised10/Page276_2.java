package Exercised10;

import java.util.Random;

public class Page276_2 {
	public static int random(int a) {
		return (int) (Math.random()*a);
	}

	public static int random(int a, int b) {
		Random r = new Random();
		int Result = r.nextInt(b-a) + a;
		return Result;
	}

	public static double random(double a, double b) {
		return ((Math.random() * a) + b);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(random(10));
		System.out.println(random(10,20));
		System.out.println(random(5.3,6.8));
	}

}
