package Exercised10;

public class Page276_3 {

	public static boolean isPrime(int a) {
		int count = 0;
		int i = 2;
		while (i < a) {
			if (a % i == 0) {
				count++;
				break;
			}
			i++;
		}

		if (count > 0) {
			return false;
		} else {
			return true;
		}
	}

	public static void main(String[] args) {
		System.out.println(isPrime(12));
	}

}
