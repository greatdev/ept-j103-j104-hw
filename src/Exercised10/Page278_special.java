package Exercised10;

public class Page278_special {
	public static int factorial(int n) {
		if (n == 1) {
			return 1;
		} else {
			return n * (factorial(n - 1));
		}
	}

	public static int numbersOfRegions(int n) {
		if (n == 0) {
			return 1;
		} else {
			return n+numbersOfRegions(n-1);
		}
	}
	
	public static String reverse(String t){
		if(t.length() == 0){
			return "";
		}else{
			return t.charAt(t.length()-1)+reverse(t.substring(0, t.length()-1));
		}
	}
	
	public static int log2(int n){
		return 0;
	}
	
	public static int gcd(int n, int m){
		if(n == 0){
			return m;
		}else if(m==0){
			return n;
		}else{
			return gcd(m, n%m);
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(factorial(4));
		System.out.println(reverse("ABCDE"));
		System.out.println(gcd(16,24));
	}

}
