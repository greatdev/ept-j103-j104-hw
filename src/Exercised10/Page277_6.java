package Exercised10;

import java.util.Arrays;

public class Page277_6 {
	static String sum = "";

	public static void charText(char s, int size) {

		if (size == 0) {
			switch (s) {
			case '1':
				sum += "หนึ่ง";
				break;
			case '2':
				sum += "สอง";
				break;
			case '3':
				sum += "สาม";
				break;
			case '4':
				sum += "สี่";
				break;
			case '5':
				sum += "ห้า";
				break;
			case '6':
				sum += "หก";
				break;
			case '7':
				sum += "เจ็ด";
				break;
			case '8':
				sum += "แปด";
				break;
			case '9':
				sum += "เก้า";
				break;
			case '0':
				sum += "ศูนย์";
				break;
			}
		} else {
			switch (s) {
			case '-':
				sum += "ลบ";
				break;
			case '.':
				sum += "จุด";
				break;
			case '1':
				sum += "หนึ่ง";
				break;
			case '2':
				sum += "ยี่";
				break;
			case '3':
				sum += "สาม";
				break;
			case '4':
				sum += "สี่";
				break;
			case '5':
				sum += "ห้า";
				break;
			case '6':
				sum += "หก";
				break;
			case '7':
				sum += "เจ็ด";
				break;
			case '8':
				sum += "แปด";
				break;
			case '9':
				sum += "เก้า";
				break;
			case '0':
				sum += "ศูนย์";
				break;
			}
		}
	}

	public static void unitText(int size) {
		switch (size) {
		case 5:
			sum += "หมื่น";
			break;
		case 4:
			sum += "พัน";
			break;
		case 3:
			sum += "ร้อย";
			break;
		case 2:
			sum += "สิบ";
			break;
		default:
			sum += "";
			break;
		}
	}

	public static String text(double d, int p) {
		String s = "%." + p + "f";
		String s_last = String.format(s, d);
		String s_split[] = s_last.split("\\.");
		// s_split[0] = unit, s_split[1] = decimal
		String unit = s_split[0];
		String decimal = s_split[1];

		if (unit.startsWith("-")) {
			charText(unit.charAt(0), unit.length());
			unit = unit.substring(1);
		}

		int unitSize = unit.length();
		for (int i = 0; i < unit.length(); i++) {
			charText(unit.charAt(i), unitSize);
			unitText(unitSize--);
		}
		charText('.', unit.length());
		for (int i = 0; i < decimal.length(); i++) {
			charText(decimal.charAt(i), 0);
		}

		return sum;
	}

	public static void main(String[] args) {
		System.out.println(text(-11125.342, 3));
	}

}
