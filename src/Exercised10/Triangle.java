package Exercised10;

public class Triangle {

	private double a, b, c;
	private double width, height;
	private double max = Integer.MIN_VALUE;

	public Triangle(int x1, int x2, int x3, int y1, int y2, int y3) {
		a = Math.sqrt(Math.pow((x1 - x2), 2) + Math.pow((y1 - y2), 2));
		b = Math.sqrt(Math.pow((x2 - x3), 2) + Math.pow((y2 - y3), 2));
		c = Math.sqrt(Math.pow((x1 - x3), 2) + Math.pow((y2 - y3), 2));
		
		if (a > b && a > c) {
			max = a;
			width = b;
			height = c;
		} else if (b > a && b > c) {
			max = b;
			width = a;
			height = c;
		} else {
			max = c;
			width = a;
			height = b;
		}
	}

	// type check
	//ด้านเท่า
	public boolean isEquilateral() {
		if (width == height && height == max) {
			return true;
		} else {
			return false;
		}
	}
	
	//หน้าจั่ว
	public boolean isIsosceles() {
		if (width == height || height == max || width == max) {
			return true;
		} else {
			return false;
		}
	}

	//ด้านไม่เท่า
	public boolean isScalene() {
		if (width != height || height != max || width != max) {
			return true;
		} else {
			return false;
		}
	}

	// corner check
	//มุมฉาก
	public boolean isRightangled() {
		if (width * width + height * height == max * max) {
			return true;
		} else {
			return false;
		}
	}
	
	//มุมแหลม
	public boolean isAcute() {
		if (width * width + height * height > max * max) {
			return true;
		} else {
			return false;
		}
	}
	
	//มุมป้าน
	public boolean isObtuse() {
		if (width * width + height * height > max * max) {
			return true;
		} else {
			return false;
		}
	}

}
