package Exercised10;

public class Page278_10_binary {

	// binary ของ k
	public static String what1(int x) {
		if (x == 0)
			return "";
		return what1(x / 2) + (x % 2);
	}
	
	// เรียง binary ทุกเลขจำนวน k หลัก
	public static void what2(int k){
		what2("", k);
	}
	
	private static void what2(String d, int k){
		if(k==d.length()){
			System.out.println(d);
		}else{
			what2(d+"0", k);
			what2(d+"1", k);
		}
	}

	public static void main(String[] args) {
		System.out.println(what1(4));
		System.out.println();
		what2(4);
	}
	
	

}
