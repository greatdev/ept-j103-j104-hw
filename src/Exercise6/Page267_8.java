package Exercise6;

import java.util.Arrays;
import java.util.Scanner;

public class Page267_8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.print("Insert number : ");
		double a = sc.nextDouble();
		double b[] = new double[5];
		int i = 0;
		while(a>0){
			double select = (a%1000);
			a /= 1000;
			b[i++] = select;
		}
		
		i = b.length-1;
		String s ="";
		while(i>=0){
			if(b[i]!=0){
				s += b[i]+",";
			}
			i--;
		}
		
		s = s.substring(0,s.length()-1);
		System.out.println(s);
	}

}
