package Exercise6;

import java.util.Scanner;

public class Page267_14 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.print("Input Day ");
		int d = sc.nextInt();
		System.out.print("Input Month ");
		int m = sc.nextInt();
		System.out.print("Input Year ");
		int y = sc.nextInt();

		int sum = d;

		for (int i = 1; i <= m; i++) {
			switch (i - 1) {
			// month 1
			case 0:
				sum += 0;
				break;
			case 1:
				sum += 31;
				break;
			case 2:
				if (y % 4 == 3) {
					sum += 29;
				} else {
					sum += 28;
				}
				;
				break;
			case 3:
				sum += 31;
				break;
			case 4:
				sum += 30;
				break;
			case 5:
				sum += 31;
				break;
			case 6:
				sum += 30;
				break;
			case 7:
				sum += 31;
				break;
			case 8:
				sum += 31;
				break;
			case 9:
				sum += 30;
				break;
			case 10:
				sum += 31;
				break;
			case 11:
				sum += 30;
				break;
			case 12:
				sum += 31;
				break;
			}
		}
		
		System.out.println("จำนวนวันทั้งหมด "+sum);
	}

}
