package Exercise3;

import java.util.Scanner;

public class Page254_11 {
	// พื้นที่ร่างกาย
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.print("Insert weight(kg): ");
		double w = sc.nextDouble();
		System.out.print("Insert heght(cm)");
		double h = sc.nextDouble();
		
		//Mosteller solution
		double S = Math.sqrt((w*h)/3600);
		System.out.println("Mosteller Solution = "+S);
		
		//Dubois solution
		S = (71.84*Math.pow(w, 0.425)*Math.pow(h, 0.725))/10000;
		System.out.println("Dubois Solution = "+S);
		
		//Boyd solution
		double b = 0.7285-0.0188*(3+Math.log(w));
		S = 0.0003207*Math.pow(h, 0.3)*Math.pow((1000*w), b);
		System.out.println("Boyd Solution = "+S);
	}

}
