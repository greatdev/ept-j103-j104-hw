package Exercise3;

import java.util.Scanner;

public class Page253_5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.print("Input a1-a5: ");
		int a1 = sc.nextInt();
		int a2 = sc.nextInt();
		int a3 = sc.nextInt();
		int a4 = sc.nextInt();
		int a5 = sc.nextInt();
		
		int temp = a1;
		a1 = a2;
		a2 = a3;
		a3 = a4;
		a4 = a5;
		a5 = temp;
		
		System.out.println(a1+" "+a2+" "+a3+" "+a4+" "+a5);
		
		
	}

}
