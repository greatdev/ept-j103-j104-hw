package Exercise3;

import java.util.Scanner;

public class Page253_7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.print("Insert a1: ");
		double a1 = sc.nextDouble();
		System.out.print("Insert b1: ");
		double b1 = sc.nextDouble();
		System.out.print("Insert c1: ");
		double c1 = sc.nextDouble();
		System.out.print("Insert a2: ");
		double a2 = sc.nextDouble();
		System.out.print("Insert b2: ");
		double b2 = sc.nextDouble();
		System.out.print("Insert c2: ");
		double c2 = sc.nextDouble();
		
		//step1 สมการ 1 - 2 ให้ a มีค่าเท่ากัน
		double a3 = a1*a2;
		double b3 = b1*a2;
		double c3 = c1*a2;
		
		double a4 = a2*a1;
		double b4 = b2*a1;
		double c4 = c2*a1;
		
		//step 2 หา y จาก ax+by=c ; x=0
		double y = (c4-c3)/(b4-b3);
		
		//step 3 เอา y ไปแทนในสมการ เพื่อหา x
		double x = (c1-(b1*y))/a1;
		
		System.out.println("X is "+x+", Y is "+y);
		
	}

}
