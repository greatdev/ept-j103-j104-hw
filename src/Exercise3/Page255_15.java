package Exercise3;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JFrame;

public class Page255_15 extends JFrame{

	public Page255_15() {
		// TODO Auto-generated constructor stub
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(500,500);
		setVisible(true);
	}
	
	public void paint(Graphics g){
		g.setColor(new Color(255,250,205));
		g.fillRect(0, 0, 500, 500);
		g.setColor(new Color(255,106,106));
		g.fillRect(100, 100, 70, 135);
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Page255_15();
	}

}
