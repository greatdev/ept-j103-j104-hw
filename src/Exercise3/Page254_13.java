package Exercise3;

import java.util.Scanner;

public class Page254_13 {
	
	public static double fac(double n){
		if(n==0 || n==1){
			return 1;
		}else{
			return n*fac(n-1);
		}
		
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		double n = sc.nextDouble();
		double sum = 0;
		for(long i = 0; i<= n;i++){
			sum += 1/fac(i);
		}
		
		System.out.println("e is "+sum);
	}

}
