package Exercise3;

import java.util.Scanner;

public class Page253_6 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Insert f1: ");
		double f1 = sc.nextDouble();
		double f0 = 2e10;
		
		double v = 10.7585e8*((f1-f0)/(f1+f0));
		System.out.println("Answer is "+v);
	}
}
