package Exercise3;

import java.util.Scanner;

public class Page254_10 {
	// String approximation ประมาณค่า n!
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.print("Input n! :");
		double n = sc.nextDouble();
		
		double fac_n = Math.sqrt(2*Math.PI*n)*Math.pow(n/(Math.E), n);
		
		System.out.println((int) n+"! is "+fac_n);
		
	}

}
