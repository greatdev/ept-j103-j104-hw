package Exercise3;

import java.awt.Graphics;
import java.util.Scanner;

import javax.swing.JFrame;

public class Page255_16 extends JFrame{
	int d;
	int angle;

	public Page255_16() {
		// TODO Auto-generated constructor stub
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(500,500);
	}
	
	public void paint(Graphics g){
		int x1,y1,x2,y2,x3,y3,x4,y4;
		int x1_2,y1_2,x2_2,y2_2,x3_2,y3_2,x4_2,y4_2;
		x1 = 0;
		y1 = 0;
		x2 = 0+d;
		y2 = 0;
		x3 = 0+d;
		y3 = 0+d;
		x4 = 0;
		y4 = 0+d;
		
		double a = angle*Math.PI / 180.0;
		x1_2 = 250+(int)(Math.cos(a)*x1+Math.sin(a)*y1);
		y1_2 = 250+(int)((-1)*Math.sin(a)*x1+Math.cos(a)*y1);
		
		x2_2 = 250+(int)(Math.cos(a)*x2+Math.sin(a)*y2);
		y2_2 = 250+(int)((-1)*Math.sin(a)*x2+Math.cos(a)*y2);
		
		x3_2 = 250+(int)(Math.cos(a)*x3+Math.sin(a)*y3);
		y3_2 = 250+(int)((-1)*Math.sin(a)*x3+Math.cos(a)*y3);
		
		x4_2 = 250+(int)(Math.cos(a)*x4+Math.sin(a)*y4);
		y4_2 = 250+(int)((-1)*Math.sin(a)*x4+Math.cos(a)*y4);
		
		g.drawLine(x1_2, y1_2, x2_2, y2_2);
		g.drawLine(x2_2, y2_2, x3_2, y3_2);
		g.drawLine(x3_2, y3_2, x4_2, y4_2);
		g.drawLine(x4_2, y4_2, x1_2, y1_2);
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Input d and angle : ");
		Scanner sc = new Scanner(System.in);
		Page255_16 obj = new Page255_16();
		obj.d = sc.nextInt();
		obj.angle = sc.nextInt();
		obj.setVisible(true);
	}

}
