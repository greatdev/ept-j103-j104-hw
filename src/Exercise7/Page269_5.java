package Exercise7;

import java.util.Scanner;

public class Page269_5 {
	public static void main(String[] args) {
		int a[][] = new int[3][3];
		int b[][] = new int[3][3];
		int c[][] = new int[3][3];
		int d[][] = new int[3][3];
		Scanner sc = new Scanner(System.in);
		for(int i = 0;i<a.length;i++){
			for(int j = 0; j<a[0].length;j++){
				System.out.print("insert a["+i+"]["+j+"] : " );
				a[i][j] = sc.nextInt();
			}
		}
		
		for(int i = 0;i<b.length;i++){
			for(int j = 0; j<b[0].length;j++){
				System.out.print("insert b["+i+"]["+j+"] : ");
				b[i][j] = sc.nextInt();
			}
		}
		
		//print
		System.out.println("Array a =>");
		for(int i = 0;i<a.length;i++){
			for(int j = 0; j<a[0].length;j++){
				System.out.print(a[i][j]+" ");
			}
			System.out.println();
		}
		System.out.println("Array b =>");
		for(int i = 0;i<b.length;i++){
			for(int j = 0; j<b[0].length;j++){
				System.out.print(b[i][j]+" ");
			}
			System.out.println();
		}
		
		//sum
		System.out.println("Array sum =>");
		for(int i = 0;i<a.length;i++){
			for(int j = 0; j<a[0].length;j++){
				c[i][j] = a[i][j]+b[i][j];
				System.out.print(c[i][j]+" ");
			}
			System.out.println();
		}
		
		//multi
		System.out.println("Array multi =>");
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[0].length; j++) {
				int sum = 0;
				for(int k = 0; k<a.length;k++){
					//00 00
					sum+=a[i][k]*b[k][j];
				}
				d[i][j] = sum;
			}
		}
		
		for(int i = 0;i<d.length;i++){
			for(int j = 0; j<d[0].length;j++){
				System.out.print(d[i][j]+" ");
			}
			System.out.println();
		}
		
	}
}
