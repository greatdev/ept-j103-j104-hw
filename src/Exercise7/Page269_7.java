package Exercise7;

import java.util.Arrays;
import java.util.Scanner;

public class Page269_7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int x[] = new int[20];
		Scanner sc = new Scanner(System.in);
		for(int i= 0;i<x.length;i++){
			x[i] = sc.nextInt();
		}
		
		Arrays.sort(x);
		
		int count = 1;
		for(int i = 0;i<x.length-1;i++){
			if(x[i]==x[i+1]){
				count++;
			}else{
				System.out.println("เลข "+x[i]+" ซ้ำ "+count+ " ตัว");
				count = 1;
			}
		}
		System.out.println("เลข "+x[x.length-1]+" ซ้ำ "+count+ " ตัว");
		
	}

}
