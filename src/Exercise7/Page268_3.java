package Exercise7;

import java.util.Arrays;

public class Page268_3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int x[] = new int[50];
		for(int i = 0;i<50;i++){
			x[i] = (int)(Math.random()*100);
		}
		System.out.println("Array เดิม");
		System.out.println(Arrays.toString(x));
		
		//i
		System.out.println("ข้อ i");
		for(int i = 0;i<50;i++){
			for(int j = i;j<50;j++){
				if(x[i]+x[j]==37){
					System.out.print("เลข "+ x[i]+ " ตำแหน่ง "+ i);
					System.out.println(", เลข "+ x[j]+ " ตำแหน่ง "+ j);
				}
			}
		}
		
		//ii
		System.out.println();
		System.out.println("ข้อ ii");
		int max_even = Integer.MIN_VALUE;
		for(int i = 0;i<50;i++){
			if(max_even<x[i] && x[i]%2==0){
				max_even = x[i];
			}
		}
		System.out.println("จำนวนสูงสุดเลขคู่ "+ max_even);
		
		//b
		System.out.println();
		System.out.println("ข้อ b");
		int even[] = new int[50];
		int odd[] = new int[50];
		int count_even = 0;
		int count_odd = 0;
		for(int i = 0;i<50;i++){
			if(x[i]%2==0){
				even[count_even++] = x[i];
			}else{
				odd[count_odd++]=x[i];
			}
		}
		System.out.println("เลขคู่");
		System.out.println(Arrays.toString(even));
		System.out.println("เลขคี่");
		System.out.println(Arrays.toString(odd));
		
	}

}
