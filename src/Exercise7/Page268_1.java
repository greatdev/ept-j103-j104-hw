package Exercise7;

import java.util.Arrays;

public class Page268_1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int x[] = new int[100];
		int max = Integer.MIN_VALUE;
		int min = Integer.MAX_VALUE;
		int sum = 0;
		for(int i = 0;i<100;i++){
			x[i] = (int)(Math.random()*500);
			if(max < x[i]){
				max = x[i];
			}
			
			if(min > x[i]){
				min = x[i];
			}
			sum+=x[i];
		}
		System.out.println(Arrays.toString(x));
		System.out.println("Max is "+max);
		System.out.println("Min is "+min);
		System.out.println("Avg is "+(sum/100.0));
	}

}
