package Exercise7;

import java.util.Arrays;

public class Page268_2 {
	public static void main(String[] args) {
		int x[] = new int[50];
		for(int i = 0;i<50;i++){
			x[i] = (int)(Math.random()*500);
		}
		System.out.println("Array เดิม");
		System.out.println(Arrays.toString(x));
		
		for(int i = 0;i<50;i+=2){
			// swap
			int temp = x[i];
			x[i] = x[i+1];
			x[i+1] = temp;
		}
		System.out.println("Array ใหม่");
		System.out.println(Arrays.toString(x));
	}
}
