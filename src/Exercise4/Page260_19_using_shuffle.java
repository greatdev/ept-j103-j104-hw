package Exercise4;

import java.util.Arrays;
import java.util.Collections;

public class Page260_19_using_shuffle {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// สุ่มเลข 100 ตัวไม่ซ้ำกัน
		Integer x[] = new Integer[100];
		for (int i = 0; i < x.length; i++) {
			x[i] = i;
		}
		Collections.shuffle(Arrays.asList(x));
		int count = 0;
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 25; j++) {
				System.out.print(x[count++] + "\t");
			}
			System.out.println();
		}

		System.out.println("--------------");
		// สุ่มตั้งแต่ 0 - 99เฉพาะเลขคู่
		count = 0;
		for (int i = 0; i < 100; i++) {
			if (x[i] % 2 == 0) {
				System.out.print(x[i] + "\t");
			}

		}
	}

}
