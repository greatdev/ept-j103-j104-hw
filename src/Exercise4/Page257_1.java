package Exercise4;

public class Page257_1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//3.1
		int n = 100;
		int sum = 0;
		System.out.println("this is <for> ");
		for(int i = 0 ;i<=100 ;i++){
			if(i%2==0){
				System.out.print(i+" ");
				sum+=i;
			}
		}
		System.out.println();
		System.out.println(sum);
		
		//3.2
		sum = 0;
		System.out.println("this is <while> ");
		int i = 0;
		while(i<=100){
			if(i%2==0){
				System.out.print(i+" ");
				sum+=i;
			}
			i++;
		}
		System.out.println();
		System.out.println(sum);
		
		//3.3
		sum = 0;
		System.out.println("this is <do-while> ");
		i = 0;
		do{
			if(i%2==0){
				System.out.print(i+" ");
				sum+=i;
			}
			i++;
		}while(i<=100);
		System.out.println();
		System.out.println(sum);

	}

}
