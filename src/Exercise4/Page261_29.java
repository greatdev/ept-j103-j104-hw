package Exercise4;

import java.util.Scanner;

import javax.swing.JOptionPane;

public class Page261_29 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 String a = JOptionPane.showInputDialog("Enter row ");
		 int row = Integer.parseInt(a);
//		Scanner sc = new Scanner(System.in);
//		int row = sc.nextInt();

		int space, show, numb;
		//จำนวนบรรทัด
		for (int i = 0; i < row; i++) {
			numb = 1;
			for (space = 0; space < row-i; space++) {
				System.out.print(" "); //พิมช่องว่างก่อนใส่
			}
			
			//จำนวนตัวเลขที่ต้องใส่ในแต่ละบรรทัดคือ show
			for (show = 0; show <= i; show++) {
				System.out.print(numb+" ");
				numb = numb * (i - show) / (show + 1);
			}
			System.out.println();
		}

	}

}
