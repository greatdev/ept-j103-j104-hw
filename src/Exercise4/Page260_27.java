package Exercise4;

import java.util.Scanner;

public class Page260_27 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.print("Insert student : ");
		int student = sc.nextInt();
		int score[][] = new int[5][student];

		for (int i = 0; i < score.length; i++) {
			for (int j = 0; j < score[0].length; j++) {
				// score[i][j] = sc.nextInt();
				score[i][j] = (int) (Math.random() * 100);
			}
		}

		for (int i = 0; i < score.length; i++) {
			System.out.print("Test #" + (i + 1) + "\t");
			for (int j = 0; j < score[0].length; j++) {
				System.out.print(score[i][j] + "\t");
			}
			System.out.println();
		}

		System.out.println("------------");
		// ค่าเฉลี่ยของการสอบครั้งนั้น
		int sum_test;
		for (int i = 0; i < score.length-1; i++) {
			System.out.print("Test #" + (i + 1) + "\t");
			sum_test = 0;
			for (int j = 0; j < score[0].length; j++) {
				sum_test += score[i][j]; // คะแนนของนักเรีนทุกคนในการสอบครั้งนั้น
			}
			System.out.println("ค่าเฉลี่ยของการสอบครั้งนั้น " + (sum_test / score[0].length));
		}
		
		System.out.println("------------");
		// ค่าเฉลี่ยของการสอบครั้งนั้น
		int sum_student;
		for (int i = 0; i < score[0].length; i++) {
			System.out.print("Student #" + (i + 1)+" ");
			sum_student = 0;
			for (int j = 0; j < score.length; j++) {
				sum_student += score[i][j]; // คะแนนรวมของนักเรียนแต่ละคนในการสอบทุกครั้ง
			}
			System.out.println("คะแนนเฉลี่ยของนักเรียนแต่ละคนในการสอบทุกครั้ง " + (sum_student / score.length));
		}

	}

}
