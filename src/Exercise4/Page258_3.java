package Exercise4;

import java.util.Arrays;
import java.util.Scanner;

public class Page258_3 {
	
	public static int max(int[] x){
		return x[x.length-1];
	}
	
	public static int min(int[] x){
		return x[0];
	}
	
	public static double avg(int[] x){
		int sum = 0;
		for(int i = 0;i<x.length;i++){
			sum+=x[i];
		}
		
		return (double)sum/x.length;
		
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.print("How many data you want : ");
		int n = sc.nextInt();
		
		int[] x = new int[n];
		for(int i=0;i<n;i++){
			System.out.print("Insert Data #"+(i+1)+": ");
			x[i] = sc.nextInt();
		}
		
		Arrays.sort(x);
		
		System.out.println("Max is "+max(x));
		System.out.println("Min is "+min(x));
		System.out.println("Avg is "+avg(x));
		
	}

}
