package Exercise4;

public class Page259_17 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int product[][] = new int[2][3]; // type, month

		int total = 0;
		for (int i = 0; i < product.length; i++) {
			for (int j = 0; j < product[0].length; j++) {
				product[i][j] = (int) (Math.random() * 50000);
				System.out.print(product[i][j] + "\t");
				total += product[i][j] ;
			}
			System.out.println();
		}

		System.out.println("-----------------");
		System.out.println("จำนวนยอดขายแต่ละชนิดในรอบ 3 เดือน");
		int sum;
		for (int i = 0; i < product.length; i++) {
			System.out.println("สินค้า #" + (i+1));
			sum = 0;
			for (int j = 0; j < product[0].length; j++) {
				sum += product[i][j];
			}
			System.out.println("ยอดขายในรอบสามเดือน " + sum);
		}
		System.out.println("-----------------");

		System.out.println("จำนวนยอดขายรวมทั้งสองชนิดในแต่ละเดือน");
		
		for (int j = 0; j < product[0].length; j++) {
			sum = 0;
			for (int i = 0; i < product.length; i++) {
				sum += product[i][j];
			}
			System.out.println("ยอดขายแต่ละชนิดในแต่ละเดือน " + sum);
		}
		System.out.println("-----------------");
		System.out.println("ยอดขายทั้งหมด "+total);

	}

}
