package Exercise4;

public class Page258_6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int super_market[][] = new int[7][12];
		
		for(int i = 0;i<super_market.length;i++){
			for(int j = 0;j<super_market[0].length; j++){
				super_market[i][j] = (int) Math.round(Math.random()*50000);
			}
		}
		
		// รายได้แต่ละสาขา +  แผนก
		int total = 0; //สำหรับ ทั้งsuper market
		for(int i = 0;i<super_market.length;i++){
			int sum = 0; // สำหรับแผนก
			for(int j = 0;j<super_market[0].length; j++){
				System.out.println("แผนก "+(i+1)+" สาขา "+(j+1)+" รายได้ "+super_market[i][j]);
				sum += super_market[i][j];
			}
			System.out.println("---------------------------------");
			System.out.println("แผนก "+(i+1)+", รายได้รวม "+sum);
			System.out.println("---------------------------------");
			total+=sum;
		}
		System.out.println("รายได้ทุกแผนกและสาขารวม "+total);
		
	}

}
