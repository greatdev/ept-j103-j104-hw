package Exercise4;

import java.util.Scanner;

public class Page260_26 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.print("Insert Array size : ");
		int size = sc.nextInt();
		int x[] = new int[size];
		
		for(int i = 0;i<size;i++){
			x[i] = (int)(Math.random()*1000);
			System.out.print(x[i]+"\t");
		}
		
		System.out.println();
		
		System.out.print("Insert lowerindex : ");
		int lowerindex = sc.nextInt();
		System.out.print("Insert upperindex : ");
		int upperindex = sc.nextInt();
		
		if(lowerindex<upperindex){
			int sum = 0;
			for(int i = lowerindex; i<upperindex ;i++){
				sum+=x[i];
			}
			System.out.println("sum between lowerindex and upperindex is : "+sum);
		}else{
			System.out.println("Error");
		}
	}

}
