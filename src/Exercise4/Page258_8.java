package Exercise4;

import java.util.Scanner;

public class Page258_8 {

	public static int sum(int a){
		int sum = 0;
		while(a>0){
			sum += (a%10);
			a = a/10;
		}
		return sum;
	}
	
	public static void oddAndEven(int a){
		int even = 0;
		int odd = 0;
		int temp = a;
		while(a>0){
			int unit = a%10;
			if(unit%2==0){
				even++;
			}else{
				odd++;
			}
			a = a/10;
		}
		
		System.out.println(temp+", even is "+even);
		System.out.println(temp+", odd is "+odd);
		System.out.println("----------------");
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Insert number : ");
		
		int a = sc.nextInt();
		int b = sc.nextInt();
		int c = sc.nextInt();
		System.out.println("----------------");
		//8.1
		System.out.println(a+" = "+sum(a));
		System.out.println(b+" = "+sum(b));
		System.out.println(c+" = "+sum(c));
		
		//8.2
		System.out.println("----------------");
		oddAndEven(a);
		oddAndEven(b);
		oddAndEven(c);
		
	}

}
