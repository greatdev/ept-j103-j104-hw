package Exercise4;

import java.util.Scanner;

public class Page257_2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.println("\tNo.\t\t\tTotal Sales (Baht)\t Sales Commission (Baht)");
		int total_sales;
		int i = 1;
		while(true){
			System.out.print("Insert Total Sales of #"+i+"\t\t");
			total_sales = sc.nextInt();
			if(total_sales < 0){
				System.out.println("Ending :D");
				break;
			}
			
			int commission;
			if(total_sales < 10000){
				commission = 0;
			}else if(total_sales >= 25000){
				commission = 10;
			}else{
				commission = 7;
			}

			double sales_commission = (double)total_sales*((double)commission/100);
			System.out.println("\t"+i+"\t\t\t\t"+total_sales+"\t\t\t"+sales_commission);
			i++;
		}
		
		
		
	}

}
