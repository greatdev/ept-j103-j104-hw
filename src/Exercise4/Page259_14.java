package Exercise4;

import java.util.Scanner;

public class Page259_14 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int less0 = 0;
		int sumLess = 0;
		int sumMore = 0;
		int more0 = 0;
		Scanner sc = new Scanner(System.in);
		while(true){
			System.out.print("Insert real number :");
			int n = sc.nextInt();
			if(n==0){
				break;
			}
			
			if(n<0){
				sumLess+=n;
				less0++;
			}else{
				sumMore+=n;
				more0++;
			}
		}
		
		System.out.println("sum I- = "+sumLess);
		System.out.println("sum I+ = "+sumMore);
		System.out.println("avg I- = "+sumLess/less0);
		System.out.println("avg I+ = "+sumMore/more0);
	}

}
