package Exercise4;

import java.util.Arrays;

public class Page258_7 {

	public static String day(int i) {
		switch (i) {
		case 0:
			return "จันทร์";
		case 1:
			return "อังคาร";
		case 2:
			return "พุธ";
		case 3:
			return "พฤหัสบดี";
		case 4:
			return "ศุกร์";
		default:
			return "";
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int company[][][] = new int[5][3][5];
		for (int day = 0; day < company.length; day++) {
			for (int i = 0; i < company[0].length; i++) {
				for (int j = 0; j < company[0][0].length; j++) {
					company[day][i][j] = (int) Math.round(Math.random() * 50000);
				}
			}
		}

		// 7.1
		System.out.println("###รวมรายจ่ายแต่ละสาขาวันอังคาร###");
		for (int i = 0; i < company[0].length; i++) {
			int sum = 0;
			for (int j = 0; j < company[0][0].length; j++) {
				sum += company[1][i][j]; // วันอังคาร
			}
			System.out.println("สาขา " + (i + 1) + " รายจ่ายรวม " + sum);
		}
		
		System.out.println("---------------------------");
		// 7.2
		System.out.println("###คำนวนรายจ่ายรวมของแผนกที่ 4 ในทั้ง 3 สาขา###");
		int sum = 0;
		for (int day = 0; day < company.length; day++) {
			for (int i = 0; i < company[0].length; i++) {
				sum += company[day][i][3]; // แผนก 4
			}
			System.out.println("วัน" + day(day) +" รายจ่ายรวม " + sum);
		}
		System.out.println("---------------------------");
		System.out.println("###คำนวนรายจ่ายรวมของแต่ละสาขา แล้วระบุว่าสาขาไหนรายจ่ายสูงสุด###");
		int max= Integer.MIN_VALUE;
		int index = 0; 
		for (int day = 0; day < company.length; day++) {
			for (int i = 0; i < company[0].length; i++) {
				sum = 0;
				for (int j = 0; j < company[0][0].length; j++) {
					sum+=company[day][i][j];
				}
				System.out.println("วัน"+day(day)+" แผนก #"+i+" มีรายจ่ายรวม "+sum);
				if(max < sum){
					max = sum;
					index = i;
				}
			}
		}
		
		System.out.println(">>แผนกที่ทำรายได้สูงสุดคือ แผนก​ #"+index);

	}

}
