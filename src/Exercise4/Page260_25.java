package Exercise4;

import java.util.Scanner;

public class Page260_25 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.println("Insert n : ");
		int input = sc.nextInt();
		int max = Integer.MIN_VALUE;
		int min = Integer.MAX_VALUE;
		int sum = 0;
		for(int i = 0;i<input;i++){
			System.out.print("Insert #"+(i+1)+": ");
			int n = sc.nextInt();
			sum+=n;
			if(max<n){
				max = n;
			}
			
			if(min>n){
				min = n;
			}
		}
		
		System.out.println("Max is "+max);
		System.out.println("Min is "+min);
		System.out.println("Avg is "+((double)sum/10));
	}

}
