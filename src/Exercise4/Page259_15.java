package Exercise4;

public class Page259_15 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int x[] = new int[10];
		for(int i =0;i<10;i++){
			x[i] = (int)(Math.random()*100);
			System.out.print(x[i]+"\t");
		}
		
		int maxEven = Integer.MIN_VALUE;
		int maxOdd = Integer.MIN_VALUE;
		int minEven = Integer.MAX_VALUE;
		int minOdd = Integer.MAX_VALUE;
		for(int i =0;i<10;i++){
			if(x[i]%2==0){
				if(maxEven < x[i]){
					maxEven = x[i];
				}
				
				if(minEven > x[i]){
					minEven = x[i];
				}
			}else{
				if(maxOdd < x[i]){
					maxOdd = x[i];
				}
				
				if(minOdd > x[i]){
					minOdd = x[i];
				}
			}
		}
		
		System.out.println();
		System.out.println("Max Even "+ maxEven);
		System.out.println("Min Even "+ minEven);
		System.out.println("Max Odd "+ maxOdd);
		System.out.println("Min Odd "+ minOdd);
		
	}

}
