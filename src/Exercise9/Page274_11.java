package Exercise9;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Page274_11 {

	public static void main(String[] args){
		// TODO Auto-generated method stub
		URL url;
		try {
			url = new URL("http://rss.cnn.com/rss/cnn_world.rss");
			BufferedReader r = new BufferedReader(new InputStreamReader(url.openStream()));
			Scanner sc = new Scanner(r);
			String content = "";
			while (sc.hasNext()) {
				String s = sc.next();
				content += s;
				if (s == null) {
					break;
				}
			}
			sc.close();
			
			Pattern p = Pattern.compile("<title><!(.*?)>");
			Matcher m = p.matcher(content);
			while(m.find()){
				String matchGroup = m.group(1);
				String ss = matchGroup.substring(7, (matchGroup.length()-2));
				System.out.println(ss);
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}

}
