package Exercise9;

public class Page273_9_palindrome_vowel {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String s= "A man, a plan, acanal. Panama";
		s = s.toUpperCase();
		
		s= s.replaceAll("[^A-Z]", "");
		
		int size= s.length()-1;
		boolean palindrome = true;
		for(int i = 0 ;i<size/2;i++){
			if(s.charAt(i) != s.charAt(size--)){
				palindrome = false;
				break;
			}
		}
		
		if(palindrome == true){
			System.out.println("This is palindrome");
		}else{
			System.out.println("It's not");
		}
	}

}
