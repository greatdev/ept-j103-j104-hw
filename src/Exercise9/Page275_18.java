package Exercise9;

import java.util.Scanner;

public class Page275_18 {

	static char thaiToEng(char s) {
		switch (s) {
		case 'ก':
			return 'd';
		case 'ข':
			return '-';
		case 'ค':
			return '8';
		case 'ฆ':
			return 'S';
		case 'ง':
			return '\'';
		case 'จ':
			return '0';
		case 'ฉ':
			return 'C';
		case 'ช':
			return '=';
		case 'ซ':
			return ':';
		case 'ญ':
			return 'P';
		case 'ฎ':
			return 'E';
		case 'ฏ':
			return 'D';
		case 'ฐ':
			return '{';
		case 'ฑ':
			return 'R';
		case 'ฒ':
			return '<';
		case 'ณ':
			return 'I';
		case 'ด':
			return 'f';
		case 'ต':
			return '9';
		case 'ถ':
			return '5';
		case 'ท':
			return 'm';
		case 'ธ':
			return 'T';
		case 'น':
			return 'o';
		case 'บ':
			return '[';
		case 'ป':
			return 'x';
		case 'ผ':
			return 'z';
		case 'ฝ':
			return '/';
		case 'พ':
			return 'r';
		case 'ฟ':
			return 'a';
		case 'ภ':
			return '4';
		case 'ม':
			return ',';
		case 'ย':
			return 'p';
		case 'ร':
			return 'i';
		case 'ฤ':
			return 'A';
		case 'ว':
			return ';';
		case 'ศ':
			return 'L';
		case 'ษ':
			return 'K';
		case 'ส':
			return 'l';
		case 'ห':
			return 's';
		case 'ฬ':
			return '>';
		case 'อ':
			return 'v';
		case 'ฮ':
			return 'V';
		case 'ฯ':
			return 'O';
		case 'ะ':
			return 't';
		case 'ั':
			return 'y';
		case 'า':
			return 'k';
		case 'ำ':
			return 'e';
		case 'ิ':
			return 'b';
		case 'ี':
			return 'u';
		case 'ึ':
			return '7';
		case 'ื':
			return 'n';
		case 'ุ':
			return '6';
		case 'ู':
			return '^';
		case '้':
			return 'h';
		case '๊':
			return 'U';
		case '๋':
			return 'J';
		case '์':
			return 'N';
			
		case 'เ':
			return 'g';
		case 'แ':
			return 'c';
		case 'โ':
			return 'F';
		case 'ใ':
			return '.';
		case 'ไ':
			return 'w';
		case 'ๆ':
			return 'q';
			
		case '๑':
			return '@';
		case '๒':
			return '#';
		case '๓':
			return '$';
		case '๔':
			return '%';
		case '๕':
			return '*';
		case '๖':
			return '(';
		case '๗':
			return ')';
		case '๘':
			return '_';
		case '๙':
			return '+';
		default:
			return ' ';
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		String s = sc.nextLine();
		for(int i = 0;i<s.length() ;i++){
			System.out.print(thaiToEng(s.charAt(i)));
		}
	}

}
