package Exercise9;

import java.util.Scanner;

public class Page274_16 {
	
	public static String plural(String s){
		if(s.endsWith("s") || s.endsWith("x") || s.endsWith("ch")){
			s += "es";
		}else if(s.endsWith("y") && !Character.toString(s.charAt(s.length()-2)).matches("[a,e,i,o,u]") ){
			s = s.substring(0,s.length()-1);
			s += "ies";
		}else{
			s += "s";
		}
		return s;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.println("Insert String :");
		String s = sc.nextLine();
		System.out.println(plural(s));
		
		
		
	}

}
