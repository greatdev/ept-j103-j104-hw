package Exercise9;

import java.util.Scanner;

public class Page275_17 {

	static char engToThai(char s) {
		switch (s) {
		case 'a':
			return 'ฟ';
		case 'b':
			return 'ิ';
		case 'c':
			return 'แ';
		case 'd':
			return 'ก';
		case 'e':
			return 'ำ';
		case 'f':
			return 'ด';
		case 'g':
			return 'เ';
		case 'h':
			return '้';
		case 'i':
			return 'ร';
		case 'j':
			return '่';
		case 'k':
			return 'า';
		case 'l':
			return 'ส';
		case 'm':
			return 'ท';
		case 'n':
			return 'ื';
		case 'o':
			return 'น';
		case 'p':
			return 'ย';
		case 'q':
			return 'ๆ';
		case 'r':
			return 'พ';
		case 's':
			return 'ห';
		case 't':
			return 'ะ';
		case 'u':
			return 'ี';
		case 'v':
			return 'อ';
		case 'w':
			return 'ไ';
		case 'x':
			return 'ป';
		case 'y':
			return 'ั';
		case 'z':
			return 'ผ';
		case '1':
			return 'ๅ';
		case '2':
			return '/';
		case '3':
			return '_';
		case '4':
			return 'ภ';
		case '5':
			return 'ถ';
		case '6':
			return 'ุ';
		case '7':
			return 'ึ';
		case '8':
			return 'ค';
		case '9':
			return 'ต';
		case '0':
			return 'จ';
		case 'A':
			return 'ฤ';
		case 'B':
			return 'ฺ';
		case 'C':
			return 'ฉ';
		case 'D':
			return 'ฏ';
		case 'E':
			return 'ฎ';
		case 'F':
			return 'โ';
		case 'G':
			return 'ฌ';
		case 'H':
			return '็';
		case 'I':
			return 'ณ';
		case 'J':
			return '๋';
		case 'K':
			return 'ษ';
		case 'L':
			return 'ศ';
		case 'M':
			return '?';
		case 'N':
			return '์';
		case 'O':
			return 'ฯ';
		case 'P':
			return 'ญ';
		case 'Q':
			return '๐';
		case 'R':
			return 'ฑ';
		case 'S':
			return 'ฆ';
		case 'T':
			return 'ธ';
		case 'U':
			return '๊';
		case 'V':
			return 'ฮ';
		case 'W':
			return '"';
		case 'X':
			return ')';
		case 'Y':
			return 'ํ';
		case 'Z':
			return '(';
		default:
			return ' ';
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		String s = sc.nextLine();
		for (int i = 0; i < s.length(); i++) {
			System.out.print(engToThai(s.charAt(i)));
		}
	}

}
