package Exercise9;

import java.util.Scanner;

public class Page273_8_id_card {

	public static String lastNumb(String s){
		int sum = 0;
		int n = s.length();
		
		for(int i = 0; i<n ;i++){
			sum += (14-i)*Character.getNumericValue(s.charAt(i));
		}
		
		return (11-(sum%11))%10+"";
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		String id_card = sc.next();
		String last_numb = lastNumb(id_card);
		System.out.println(id_card+last_numb);
	}
}
