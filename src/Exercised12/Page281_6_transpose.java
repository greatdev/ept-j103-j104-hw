package Exercised12;

public class Page281_6_transpose {

	public static double[][] transposeMatrix(double[][] m) {
		double[][] temp = new double[m[0].length][m.length];
		for (int i = 0; i < m.length; i++)
			for (int j = 0; j < m[0].length; j++)
				temp[j][i] = m[i][j];
		return temp;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double[][] x = {{1,2,3},{4,5,6},{7,8,9}};
		for(int i = 0;i<x.length;i++){
			for(int j = 0; i<x[0].length;j++){
				System.out.println(x[i][j]+" ");
			}
		}
		
	}

}
