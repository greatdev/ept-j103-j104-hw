package Exercised12;

public class Page282_16_puzzle15 {

	public static boolean isLegal15Puzzle(int[][] b) {
		int[] puzzle = new int[15];
		int count = 0;
		int B = 0;
		for (int i = 0; i < b.length; i++) {
			for (int j = 0; j < b[0].length; j++) {
				if (b[i][j] == 0) {
					B = i;
					continue;
				}
				puzzle[count++] = b[i][j];
			}
		}

		int L = 0;
		for (int i = 0; i < puzzle.length; i++) {
			for (int j = i; j < puzzle.length; j++) {
				if (puzzle[i] > puzzle[j]) {
					L++;
				}
			}
		}

		if ((L + B) % 2 == 0) {
			return true;
		} else {
			return false;
		}

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[][] x = new int[][]{{1,2,3,4},{5,6,7,8},{9,10,12,15},{13,14,0,11}};
		int[][] y = new int[][]{{1,2,3,4},{5,6,7,8},{9,10,11,12},{13,15,14,0}};
		System.out.println(isLegal15Puzzle(x));
	}

}
