package Exercised12;

import java.util.Arrays;

public class Page282_11 {
	public static void BubbleSort(int[] num) {
		int j;
		boolean flag = true; // set flag to true to begin first pass
		int temp; // holding variable

		while (flag) {
			flag = false; // set flag to false awaiting a possible swap
			for (j = 0; j < num.length - 1; j++) {
				if (num[j] > num[j + 1]) // change to > for ascending sort
				{
					temp = num[j]; // swap elements
					num[j] = num[j + 1];
					num[j + 1] = temp;
					flag = true; // shows a swap occurred
				}
			}
		}
	}

	public static void main(String[] args) {
		int[] x = new int[]{8,2,9,5,6,1,0};
		BubbleSort(x);
		System.out.println(Arrays.toString(x));
	}
}
