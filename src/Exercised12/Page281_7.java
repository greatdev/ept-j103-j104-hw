package Exercised12;

import java.util.Arrays;

public class Page281_7 {
	
	public static int[] maxmin(int[] d){
		int max = Integer.MAX_VALUE;
		int min = Integer.MIN_VALUE;
		for(int i = 0;i < d.length;i++){
			if(max > d[i]){
				max = d[i];
			}
			
			if(min < d[i]){
				min = d[i];
			}
		}
		
		int[] arr = new int[2];
		arr[1] = max;
		arr[0] = min;
		return arr;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int x[] = new int[]{3,2,4,1,5,34,0};
		System.out.println(Arrays.toString(maxmin(x)));
	}

}
