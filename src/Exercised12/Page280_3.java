package Exercised12;

import java.util.Arrays;
import java.util.Random;

public class Page280_3 {
	
	public static int count(int[] d, int x){
		int count = 0;
		for(int i = 0; i< d.length; i++){
			if(d[i]==x){
				count++;
			}
		}
		return count;
	}
	
	public static int mode(int[] d){
		int max = Integer.MIN_VALUE;
		int max_item = 0;
		for(int i = 0; i<d.length; i++){
			int item = count(d, d[i]);
			if(max < item){
				max = item;
				max_item = d[i];
			}
		}
		return max_item;
	}
	
	public static int modeSize(int[] d){
		int max = Integer.MIN_VALUE;
		for(int i = 0; i<d.length; i++){
			int item = count(d, d[i]);
			if(max < item){
				max = item;
			}
		}
		return max;
	}
	
	public static boolean majority(int[] d){
		if(modeSize(d) > d.length/2){
			return true;
		}else{
			return false;
		}
	}
	
	public static void insert(int[] d, int i, int x){
		for(int j = d.length-1 ;j>=i ; j--){
			d[j] = d[j-1];
			if(j==i){
				d[i] = x;
				break;
			}
		}
	}
	
	public static void remove(int[] d, int i){
		for(int j = i; i<d.length-1 ;i++){
			d[i] = d[i+1];
		}
		d[d.length-1] = 0;
	}
	
	public static void shuffle(int[] d){
		int index, temp;
	    Random random = new Random();
	    for (int i = d.length - 1; i > 0; i--)
	    {
	        index = random.nextInt(i + 1);
	        temp = d[index];
	        d[index] = d[i];
	        d[i] = temp;
	    }
	}
	
	public static int[] shuffleNewArr(int[] d){
		int x[] = d;
		int index, temp;
	    Random random = new Random();
	    for (int i = x.length - 1; i > 0; i--)
	    {
	        index = random.nextInt(i + 1);
	        temp = x[index];
	        x[index] = x[i];
	        x[i] = temp;
	    }
	    return x;
	}
	
	public static boolean isSorted(double[] d){
		//น้อยไปหามาก
		double max = Integer.MIN_VALUE;
		int count = 0;
		for(int i = 0; i< d.length; i++){
			if(max < d[i]){
				max = d[i];
				count++;
			}
		}
		
		if(count == d.length){
			return true;
		}else{
			return false;
		}
	}
	
	public static boolean isSorted(String d){
		//น้อยไปหามาก
		int max = Integer.MIN_VALUE;
		int count = 0;
		for(int i = 0; i< d.length(); i++){
			if(max < (int) d.charAt(i)){
				max =(int) d.charAt(i);
				count++;
			}
		}
		
		if(count == d.length()){
			return true;
		}else{
			return false;
		}
	}
	
	public static double[] inRange(double[] d, int min, int max){
		double[] x = new double[(max-min)+1];
		int count = 0;
		for(int i = min ;i <=max ;i++){
			x[count++] = d[i]; 
		}
		return x;
	}
	
	public static boolean isZero(double[] d){
		for(int i = 0;i<d.length;i++){
			if(d[i]!=0){
				return false;
			}
		}
		return true;
	}
	
	public static boolean isIdentify(double[][] d){
		for(int i = 0;i < d.length ;i++){
			if(d[i][i] != 1){
				return false;
			}
		}
		return true;
	}
	
	public static boolean isLowerTriangular(double[][] d){
		for(int i = 0 ; i < d.length; i++){
			for(int j = i; j<d.length; j++){
				if(d[i][j] != 0){
					return false;
				}
			}
		}
		return true;
	}
	
	public static boolean isUpperTriangular(double[][] d){
		for(int i = 0 ; i < d.length; i++){
			for(int j = 0; j <= i; j++){
				if(d[i][j] != 0){
					return false;
				}
			}
		}
		return true;
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double[] x = new double[]{1,2,3,4,5,6,7};
//		System.out.println(Arrays.toString(shuffleNewArr(x)));
		System.out.println(Arrays.toString(inRange(x,2,3)));
	}

}
