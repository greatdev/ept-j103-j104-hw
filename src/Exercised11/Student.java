package Exercised11;

public class Student {
	
	private int id;
	private String name;
	private String surname;
	private int score;
	private char grade;
	
	public Student(int id, String name, String surname, int score) {
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.score = score;
	}
	
	public void gradeScore(){
		if(this.score >= 80){
			grade = 'A';
		}else if(this.score >= 70 && this.score < 80){
			grade = 'B';
		}else if(this.score >= 60 && this.score < 70){
			grade = 'C';
		}else if(this.score >= 50 && this.score < 60){
			grade = 'D';
		}else{
			grade = 'F';
		}
	}
	
	public char getGrade(){
		return grade;
	}
	
	public int getScore(){
		return this.score;
	}
	
	public String getName(){
		return this.name;
	}
	
	public String toString(){
		return "รหัส : "+id+", ชื่อ-นามสกุล :"+name+" "+surname+", score : "+score+", grade : "+grade;
	}

}
