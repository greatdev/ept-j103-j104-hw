package Exercised11;

public class Page280_4 {
	
	private double a,b;
	
	public Page280_4(double a, double b) {
		this.a = a;
		this.b = b;
	}	
	
	public double sum(){
		return a+b;
	}
	
	public double minus(){
		return a-b;
	}
	
	public double multi(){
		return a*b;
	}
	
	public double divide(){
		return a/b;
	}
	
	public double mod(){
		return a%b;
	}

}
