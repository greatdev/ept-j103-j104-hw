package Exercised11;

public class Page279_1_StudentTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Student s[] = new Student[10];
		for(int i = 0 ;i<10;i++){
			String name = "name"+i;
			String surname = "surname"+i;
			s[i] = new Student(i, name, surname, (int)(Math.random()*100) );
			s[i].gradeScore();
			System.out.println(s[i].toString());
		}
		
		int max = Integer.MIN_VALUE;
		int min = Integer.MAX_VALUE;
		int max_index = 0, min_index = 0;
		int sum = 0;
		for(int i = 0; i<10 ;i++){
			if(max < s[i].getScore()){
				max = s[i].getScore();
				max_index = i;
				
			}
			
		
			if(min > s[i].getScore()){
				min = s[i].getScore();
				min_index = i;
			}
			sum += s[i].getScore(); 
		}
		System.out.println("Max is "+ s[max_index].getName());
		System.out.println("Min is "+ s[min_index].getName());
		System.out.println("Avg Score is "+ sum/10.0);
		
		
		
	}

}
