package Exercise5;

public class Page263_6_7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		long start, stop;
		
		
		int i = 3;
		double PI = 1;
		int count = 0;
		//6
		start = System.currentTimeMillis();
		while(true){
			
			if(count%2==0){
				PI -= (1.0/(i));
			}else{
				PI += (1.0/(i));
			}
			
			if(4*PI >= 3.141592 && 4*PI <= 3.141593){
				break;
			}
			
			i+=2;
			count++;
		}
		stop = System.currentTimeMillis();
		System.out.println("ข้อ 6 พจน์ที่ "+count);
		System.out.println("ใช้เวลา "+(stop-start)+" ms");
		
		
		//7
		i = 2;
		double j = 1;
		PI = 1;
		count = 0;
		
		start = System.currentTimeMillis();
		while(true){
			
			if(count%2==0){
				j+=2; 
				PI *= i/j;
				i+=2; 
			}else{
				PI *= i/j; 
			}
			
			if(4*PI >= 3.141592 && 4*PI <= 3.141593){
				break;
			}
			count++;
		}
		stop = System.currentTimeMillis();
		System.out.println("ข้อ 7 พจน์ที่ "+count);
		System.out.println("ใช้เวลา "+(stop-start)+" ms");
	}

}
