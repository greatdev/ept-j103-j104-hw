package Exercise5;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.ListIterator;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class Page265_11_follow_mouse extends JPanel
{
	LinkedList<Point> points = new LinkedList<Point>();
	public Page265_11_follow_mouse()
	{
		Thread t = new Thread(new Runnable()
		{
			public void run()
			{
				while (true)
				{ 
					try
					{
						Thread.sleep(20);
					} catch (InterruptedException e)
					{
					}
					
					 double V = 10;
					 double size = Math.sqrt( (x- mouse_x)*(x- mouse_x) + (y- mouse_y)*(y- mouse_y) );
					 if(size < 0.01) size= 0.01;
					 x = (int)(x + V* (mouse_x-x ) / size);
					 y = (int)(y + V* (mouse_y-y ) / size);
					 points.add(new Point(x,y));
					 
					 if(points.size() > 200)points.remove(0);
					repaint();
				}
			}
		});
		t.start();
		
		addMouseMotionListener(new MouseMotionListener()
		{ 
			@Override
			public void mouseMoved(MouseEvent e)
			{
				 mouse_x = e.getX();
				 mouse_y = e.getY();
				
			} 
			@Override
			public void mouseDragged(MouseEvent arg0)
			{   }
		});
		JFrame f = new JFrame();
		f.add(this);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setSize(300, 200);
		f.setVisible(true);
		this.setBackground(Color.white);
	}

	int mouse_x,mouse_y;
	int x,y;
	public void paint(Graphics g)
	{
		super.paint(g);
		g.setColor(Color.black);
		g.fillOval(x-10, y-10, 20, 20);
		ListIterator<Point > its =  points.listIterator();
		
		for(int i=0;its.hasNext();i++)
		{
			Point p =its.next();
			int color_now =255- 255 * i /points.size();
			Color c = new Color( color_now,color_now,color_now  );
			g.setColor(c);
			g.fillOval(p.x-10, p.y-10, 20, 20);
		}
	}
	public static void main(String[] args)
	{
		new Page265_11_follow_mouse();

	}

}