package Exercise5;

import java.awt.Graphics;
import java.util.Scanner;

import javax.swing.JFrame;
//คิดไม่ออก
public class Page263_9 extends JFrame {
	double d;
	double angle;

	public Page263_9() {
		// TODO Auto-generated constructor stub
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(500, 500);
	}

	public void paint(Graphics g) {
		double x1, y1, x2, y2, x3, y3, x4, y4;
		int x1_2, y1_2, x2_2, y2_2, x3_2, y3_2, x4_2, y4_2;
	
		for (int i = 0; i < 10; i++) {
			x1 = i;
			y1 = i;
			x2 = i + d;
			y2 = i;
			x3 = i + d;
			y3 = i + d;
			x4 = i;
			y4 = i + d;

			double a = angle * Math.PI / 180.0;
			x1_2 = 250 + (int) (Math.cos(a) * x1 + Math.sin(a) * y1);
			y1_2 = 250 + (int) ((-1) * Math.sin(a) * x1 + Math.cos(a) * y1);

			x2_2 = 250 + (int) (Math.cos(a) * x2 + Math.sin(a) * y2);
			y2_2 = 250 + (int) ((-1) * Math.sin(a) * x2 + Math.cos(a) * y2);

			x3_2 = 250 + (int) (Math.cos(a) * x3 + Math.sin(a) * y3);
			y3_2 = 250 + (int) ((-1) * Math.sin(a) * x3 + Math.cos(a) * y3);

			x4_2 = 250 + (int) (Math.cos(a) * x4 + Math.sin(a) * y4);
			y4_2 = 250 + (int) ((-1) * Math.sin(a) * x4 + Math.cos(a) * y4);
			
			g.drawLine(x1_2, y1_2, x2_2, y2_2);
			g.drawLine(x2_2, y2_2, x3_2, y3_2);
			g.drawLine(x3_2, y3_2, x4_2, y4_2);
			g.drawLine(x4_2, y4_2, x1_2, y1_2);

			d = d * 0.8;
			angle += 10;
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Input d and angle : ");
		Scanner sc = new Scanner(System.in);
		Page263_9 obj = new Page263_9();
		obj.d = sc.nextInt();
		obj.angle = sc.nextInt();
		obj.setVisible(true);
	}

}
