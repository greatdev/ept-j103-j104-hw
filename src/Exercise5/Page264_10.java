package Exercise5;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class Page264_10 extends JPanel{
	int x = 0;
	public Page264_10(){
		Thread t  = new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				while(true){
					x++;
					
					try {
						Thread.sleep(20);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					repaint();
				}
			}
		});
		
		t.start();
		JFrame f = new JFrame();
		f.add(this);
		f.setDefaultCloseOperation(f.EXIT_ON_CLOSE);
		f.setSize(300, 200);
		f.setVisible(true);
		this.setBackground(Color.WHITE);
	}
	
	public void paint(Graphics g){
		super.paint(g);
		int y_now = (int) (Math.sin(x*Math.PI/this.getWidth()*2)*(this.getHeight()/2)+this.getHeight()/2);
		g.fillOval(x-10, y_now-10, 20, 20);
		int xx = 0;
		int yy = 0;
		for(int i = 0; i<this.getWidth() ;i++){
			int xxx = xx+1;
			int yyy = (int) (Math.sin((xx+1)*Math.PI/this.getWidth()*2)*(this.getHeight()/2)+this.getHeight()/2);
			g.drawLine(xx, yy, xxx, yyy);
			xx = xxx;
			yy = yyy;
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Page264_10();
	}

}
