package Exercise5;

import java.awt.Graphics;

import javax.swing.JFrame;

public class Page263_8 extends JFrame{
	
	public Page263_8(){
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(500,500);
		setVisible(true);
	}
	
	public void paint(Graphics g){
		super.paint(g);
		for(int i = 0; i<100;i++){
			for(int j = 0; j<100;j++){
				g.drawRect(j*50, i*20, 50, 20);
			}
			i++;
			for(int j = 0;j<100;j++){
				g.drawRect(j*50+25, i*20, 50, 20);
			}
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Page263_8();
	}

}
