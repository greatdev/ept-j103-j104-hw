package Exercise5;

public class Page263_4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double n = 1e8;
		int count = 0;
		while (n > 0) {
			count = 0;
			int i = 2;
			while (i < n) {
				if (n % i == 0) {
					count++;
					break;
				}
				i++;
			}
			if (count == 0) {
				System.out.println("จำนวนเฉพาะที่มีค่ามากที่สุดที่น้อยกว่า 1e8 คือ " + n);
				break;
			}
			
			n--;
		}
	}

}
