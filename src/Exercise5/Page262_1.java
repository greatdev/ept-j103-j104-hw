package Exercise5;

public class Page262_1 {
	public static void main(String[] args) {
		// 1
		int n = 10;
		int k = 0;
		int count = 0;
		while (true) {
			if (k >= n)
				break;
			System.out.print("*");
			count++;
			k++;
		}
		System.out.println(" > ข้อ 1 มี * " + count + " ตัว");

		// 2
		n = 10;
		k = n;
		count = 0;
		while (true) {
			if (k <= 0)
				break;
			System.out.print("*");
			count++;
			k--;
		}
		System.out.println(" > ข้อ 2 มี * " + count + " ตัว");

		// 3
		n = 10;
		k = 0;
		count = 0;
		while (true) {
			System.out.print("*");
			count++;
			k++;
			if (k >= n)
				break;
		}
		System.out.println(" > ข้อ 3 มี * " + count + " ตัว");

		// 4
		n = 10;
		k = n;
		count = 0;
		while (true) {
			System.out.print("*");
			count++;
			k--;
			if (k <= 0)
				break;
		}
		System.out.println(" > ข้อ 4 มี * " + count + " ตัว");
	}
}
