package Exercise2;

import java.util.Scanner;

public class Page252_7 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Input weight : ");
		double weight = sc.nextDouble();

		if (weight > 3) {
			System.out.println("คิดค่าจัดส่ง 100 บาท");
		} else if (weight <= 3 && weight > 2) {
			System.out.println("คิดค่าจัดส่ง 70 บาท");
		} else if (weight <= 2 && weight > 1) {
			System.out.println("คิดค่าจัดส่ง 60 บาท");
		} else {
			System.out.println("คิดค่าจัดส่ง 50 บาท");
		}
	}

}
