package Exercise2;

import java.util.Scanner;

public class Page251_1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int hour, minute;
		while (true) {
			System.out.print("Input hour and minute : ");
			hour = sc.nextInt();
			minute = sc.nextInt();
			if (hour >= 0 && hour < 24 && minute >= 0 && minute < 60) {
				break;
			} else {
				System.out.println("Please input correct hour(0-23) and minute(0-59)");
			}
		}

		// think about hour ชั่วโมงแรกไม่คิดค่าจอด ชั่วโมงต่อไป 30
		int count = 0;
		if (hour > 0 && minute > 0) {
			count = hour;
		}

		System.out.print("ค่าจอดรถเท่ากับ ");
		System.out.println(count * 30 + " บาท");
	}

}
