package Exercise2;

import java.util.Scanner;

public class Page251_2 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Input Score : ");
		int score = sc.nextInt();
		
		if(score>=90){
			System.out.println("You Got A");
		}else if(score<90 && score>=80){
			System.out.println("You Got B");
		}else if(score<80 && score>=70){
			System.out.println("You Got C");
		}else if(score<70 && score>=60){
			System.out.println("You Got D");
		}else{
			System.out.println("You Got F");
		}
		
	}
}
