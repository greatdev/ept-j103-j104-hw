package Exercise8;

import java.util.ArrayList;
import java.util.Scanner;

public class Page271_16 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.print("input n: ");
		int n = sc.nextInt();
		ArrayList<String> s = new ArrayList<String>();
		while(n >= 0){
			System.out.print("input string : ");
			String t = sc.nextLine();
			s.add(t);
			n--;
		}
		
		int max = Integer.MIN_VALUE;
		String string_max = "";
		for(int i = 0; i < s.size() ;i++){
			if(max < s.get(i).length()){
				max = s.get(i).length();
				string_max = s.get(i);
			}
		}
		
		System.out.println(string_max);
	}

}
