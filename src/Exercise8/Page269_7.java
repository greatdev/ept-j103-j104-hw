package Exercise8;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Page269_7 {

	public static void main(String[] args) throws FileNotFoundException {
		int eg = 0;
		int to = 0;
		int tri = 0;
		int jatawa = 0;
		Scanner sc = new Scanner(new File("/Users/GaoEy/Documents/EclipseWorkspace/HWJ103/file/Page269_7.txt"));
		while (sc.hasNextLine()) {

			String s = sc.nextLine();

			if (s.startsWith("#")) {
				continue;
			}

			for (int i = 0; i < s.length(); i++) {
				if (s.charAt(i) == '่') {
					eg++;
				} else if (s.charAt(i) == '้') {
					to++;
				} else if (s.charAt(i) == '๊') {
					tri++;
				} else if (s.charAt(i) == '๋') {
					jatawa++;
				}
			}
		}
		
		System.out.println("มีเอก "+eg+" ตัว , มีโท "+to+" ตัว , มีตรี "+tri+" ตัว , มีจัตวา "+jatawa+" ตัว");
		
		if(eg > to && eg > tri && eg > jatawa){
			System.out.println("ที่มากที่สุดคือเอก");
		}else if(to > eg && to > tri && to > jatawa){
			System.out.println("ที่มากที่สุดคือโท");
		}else if(tri > eg && tri > to && tri > jatawa){
			System.out.println("ที่มากที่สุดคือตรี");
		}else if(jatawa > eg && jatawa > to && jatawa > tri){
			System.out.println("ที่มากที่สุดคือจัตวา");
		}
		

	}

}
