package Exercise8;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Page269_5 {

	public static void search(File f, String name) throws FileNotFoundException {
		Scanner sc = new Scanner(f);
		boolean found = false;
		while (sc.hasNext()) {
			String s[] = sc.nextLine().split(",");
			if (s[0].equals(name)) {
				System.out.println("Name : " + s[0]);
				System.out.println("Surname :" + s[1]);
				System.out.println("Gender :" + s[2]);
				System.out.println("Tel :" + s[3]);
				System.out.println("Email :" + s[4]);
				found = true;
				break;
			}
		}

		if (found == false) {
			System.out.println("Doesn't have this name in file");
		}
	}

	public static void main(String[] args) throws FileNotFoundException {
		// TODO Auto-generated method stub
		File f = new File("/Users/GaoEy/Documents/EclipseWorkspace/HWJ103/file/Page269_5.txt");
		FileOutputStream outs = new FileOutputStream(f, true);
		PrintWriter pw = new PrintWriter(outs);
		Scanner sc = new Scanner(System.in);
		System.out.print("Name : ");
		String name = sc.nextLine();
		System.out.print("Surname : ");
		String surname = sc.nextLine();
		System.out.print("Gender : ");
		String gender = sc.nextLine();
		System.out.print("Tel : ");
		String tel = sc.nextLine();
		System.out.print("Email : ");
		String email = sc.nextLine();

		pw.write(name + "," + surname + "," + gender + "," + tel + "," + email + "\n");

		System.out.print("Input Name you want to searh : ");
		String search_name = sc.nextLine();
		search(f, search_name);
		pw.close();
	}

}
