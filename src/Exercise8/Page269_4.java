package Exercise8;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Page269_4 {

	public static void main(String[] args) throws FileNotFoundException {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(new File("/Users/GaoEy/Documents/EclipseWorkspace/HWJ103/file/Page269_2.txt"));
		int count = 0;
		ArrayList<Character> x = new ArrayList<Character>();
		while (sc.hasNext()) {
			String s = sc.nextLine().toUpperCase();
			for (int i = 0; i < s.length(); i++) {
				if (s.charAt(i) != ' ') {
					x.add(s.charAt(i));
					count++;
				}
			}
		}

		while(x.size() > 1){
			
			char s_index = x.get(0);
			int s_count = 0;

			int j = 0;
			while(j < x.size()){
				if (x.get(j) == s_index) {
					s_count++;
					x.remove(j);
				}else{
					j++;
				}
			}
			
			System.out.println(s_index + " มี " + s_count);
		}

		sc.close();
		System.out.println("จำนวนตัวอักษรทั้งหมด " + count);

	}

}
